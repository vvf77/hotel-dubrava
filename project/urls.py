# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import patterns, include, url

from django.contrib import admin
from filebrowser.sites import site
from django.contrib.auth.views import login, logout

# from hotel.admin import admin_site
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url('^', include('django.contrib.auth.urls')),
    url(r'booking/room/', 'hotel.views.booking_room'),
    url(r'booking/my/', 'hotel.views.my_bookings', name='my_bookings'),
    url(r'booking/$', 'hotel.views.booking_search'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^guest/register/', 'hotel.views.register'),
    url(r'^guest/login/$',  login, name="login"),
    url(r'^guest/logout/$', logout, name="logout"),
)

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}),
    )


urlpatterns += patterns('django.contrib.flatpages.views',
    url(r'^(?P<url>.*/)$', 'flatpage'),
)
