# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
import random
import datetime
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError

from django.shortcuts import render
from django import forms
from django.utils.timezone import utc
from hotel.models import Reservation, Guest, RoomType, Room
from django.contrib.flatpages.models import FlatPage
#from django.db.models.fields.related import RelatedObjectDoesNotExist
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.contrib.admin import widgets


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect("/books/")
    else:
        form = UserCreationForm()
    return render(request, "registration/register.html", {
        'form': form,
    })


def get_booking_flatpage():
    flatpage = FlatPage.objects.filter(url='/booking/').first()
    if flatpage is None:
        flatpage = FlatPage(url='/booking/')
    return flatpage


class ReservationForm(forms.Form):

    room_type = forms.ChoiceField(label=u'Номер', choices=RoomType.objects.all().values_list('id', 'name') )
    occupied_from = forms.DateField(label=u'Дата въезда', widget=widgets.AdminDateWidget())
    occupied_to = forms.DateField(label=u'Дата выезда', widget=widgets.AdminDateWidget())
    people_count = forms.IntegerField(label=u'Сколько гостей', min_value=1, max_value=6)


def check_user_exists(username):
    if User.objects.filter(username=username).exists():
        raise ValidationError(u"Пользователь с такимм логином уже есть в "
                              u"системе попробуйте добавить год рождения или как-то еще изменить.")

def check_email_exists(email):
    if User.objects.filter(email=email).exists():
        raise ValidationError(u"Пользователь с такимм емайлом есть в "
                              u"системе попробуйте войти в систему")

class ReservationNewUserForm(forms.Form):
    full_name = forms.CharField(label=u'Полное имя', max_length=250)
    email = forms.EmailField(label=u'E-mail', max_length=150, validators=[check_email_exists])
    phone = forms.CharField(label=u'Номер телефона', max_length=20)
    username = forms.CharField(label=u'Логин пользователя', max_length=20, validators=[check_user_exists])

# add validation of email to not existing in database and valid phone number

pwchars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz'

def generate_password(length=7):
    l = len(pwchars)-1
    return ''.join([pwchars[random.randint(0, l)] for i in range(length)])


@login_required
def my_bookings(request):
    """
    :param request: http request
    :return: response
    all current and future reservations of current user
    """
    guest = Guest.objects.get(user=request.user)
    today = datetime.datetime.utcnow().replace(tzinfo=utc, hour=0, minute=0, second=0)
    bookings = Reservation.objects.filter(occupied_to__gte=today, guest=guest)
    print( request.session.get('highlight_book') )
    hl_book = request.session.get('highlight_book')
    if hl_book:
        request.session['highlight_book'] = None
    return render(request, "booking/my.html", {
        "bookings": bookings,
        "guest": guest,
        "flatpage": get_booking_flatpage(),
        "highlight_book": hl_book
    })


def response_available_rooms(request, form_data, new_guest_form=None):
    flatpage = get_booking_flatpage()
    guest = None
    if request.user and not request.user.is_staff and not request.user.is_anonymous():
        guest = Guest.objects.filter(user=request.user).first()
        new_guest_form = None
    elif not new_guest_form:
        new_guest_form = ReservationNewUserForm()
    rooms = Room.available(form_data['occupied_from'], form_data['occupied_to']).filter(room_type=form_data['room_type'])
    return render(request, 'booking/available_rooms.html', {
        'rooms': rooms,
        'form': form_data,
        'flatpage': flatpage,
        'guest': guest,
        'new_guest_form': new_guest_form
    })


def booking_search(request, step=''):
    guest = None
    room_type = None
    if 'rt' in request.REQUEST:
        room_type = RoomType.objects.filter(id=request.REQUEST['rt']).first()

    if request.method == 'POST':
        form = ReservationForm(request.POST)
        if form.is_valid():
            return response_available_rooms(request, form.cleaned_data)
    else:
        initial = {'people_count': 2}
        if room_type:
            initial['room_type'] = room_type.id
            initial['people_count'] = room_type.beds

        form = ReservationForm(initial=initial)

    # if roomtype:
    #     ReservationForm.room_type.widget = forms.HiddenInput()

    flatpage = get_booking_flatpage()
    return render(request, 'booking/step1.html', {'form': form, 'flatpage': flatpage, 'roomtype': room_type})



def send_confirmed_email(booking):
    #TODO: send email about booking
    pass


def send_new_reservation_email(booking, password=None):
    pass


def booking_room(request):

    if request.GET.get('confirm') is not None:
        # here login user who come by link from e-mail
        booking = Reservation.objects.filter(confirm_code=request.GET.get('confirm')).first()
        if booking:
            booking.confirm_code = None
            booking.status = 'confirmed'
        flatpage = get_booking_flatpage()

        send_confirmed_email(booking)
        request.session.flash=""
        return HttpResponseRedirect('/booking/my/')
        # return render(request, 'booking/done.html', {'booking': booking, 'flatpage':flatpage})

    # check previous form
    form = ReservationForm(request.POST)
    room_id = [ int(k[7:]) for k in request.POST if k.startswith('roomid_')]
    if room_id:
        room_id = room_id[0]

    if not form.is_valid():
        flatpage = get_booking_flatpage()
        return render(request, 'booking/step1.html', {'form': form, 'flatpage': flatpage})

    # check if user logined and user has guest's profile
    guest = None
    if request.user and not request.user.is_anonymous():
        guest = Guest.objects.filter(user=request.user).first()

    password = None
    new_user_form = None
    if not guest:
        user = None
        new_user_form = ReservationNewUserForm(request.POST)
        if not new_user_form.is_valid() or not room_id:
            return response_available_rooms(request, form.cleaned_data, new_user_form)
        form_data = new_user_form.cleaned_data
        if request.user and not request.user.is_staff and not request.user.is_anonymous():
            user = request.user

        if user is None and form_data['username']:
            password = generate_password()
            # username = form_data['username']
            user = User.objects.create(
                email=form_data['email'],
                first_name=form_data['full_name'],
                username=form_data['username'],
                password=password,
                is_active=True)
            print("user's {} password is {}".format(form_data['username'], password))
            user.save()
            auth_backend = auth.get_backends()[0]
            user.backend = "%s.%s" % (auth_backend.__module__, auth_backend.__class__.__name__)
            auth.login(request, user)

        guest = Guest.objects.create(full_name=form_data['full_name'],
                                     email=form_data['email'],
                                     phone=form_data['phone'],
                                     user=user)
    elif not room_id:
        return response_available_rooms(request, form.cleaned_data)

    room = Room.objects.filter(id=room_id).first()
    if not room or not room.is_free(form.cleaned_data):
        print("room {} isn't free for needed dates".format(room))
        return response_available_rooms(request, form.cleaned_data, new_user_form)

    booking_form_data = form.cleaned_data
    del booking_form_data["room_type"]
    booking = Reservation(guest=guest, room_id=room_id, **form.cleaned_data)
    if guest.is_trusted:
        booking.status = 'confirmed'
        send_confirmed_email(booking)
    else:
        # TODO: generate confirm code
        # send an email to user with his password (if account was created for him)
        send_new_reservation_email(booking, password)
        pass

    booking.save()
    # render another page
    flatpage = get_booking_flatpage()
    print('Booking:{}'.format(booking))
    request.session['highlight_book'] = booking.id
    print( request.session.items() )
    return HttpResponseRedirect(reverse('my_bookings'))
    #return render(request, 'booking/done.html', {'flatpage': flatpage, 'booking': booking})

# TODO: confirming booking
# API: take free rooms of desired type

