# -*- coding: utf-8 -*-

"""
Django settings for project project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
# -*- coding: utf-8 -*-


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'i)(k&(r*r0g+x67#kdb5o87d$uz9qo_j9rt0m3p#goo02z#u-='

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'filebrowser',
    'tinymce',
    'hotel',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)

SITE_ID=1
ROOT_URLCONF = 'project.urls'

WSGI_APPLICATION = 'project.wsgi.application'
APPEND_SLASH = True

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, '../../db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = 'static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = 'media/'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, '../templates'),
)

FILEBROWSER_DIRECTORY = 'uploads/'
# FILEBROWSER_MEDIA_URL = '/media/uploads'
# DIRECTORY = 'uploads/'

TINYMCE_DEFAULT_CONFIG = {'theme': "advanced", 'relative_urls': False}

GRAPPELLI_ADMIN_TITLE = 'Пансионат "Дубрава". Администрирование.'
# FILEBROWSER_EXTENSIONS = {
#     'Folder': [''],
#     'Image': ['.jpg', '.jpeg', '.gif', '.png', '.tif', '.tiff'],
#     'Document': ['.pdf', '.doc', '.rtf', '.txt', '.xls', '.csv'],
#     'Video': ['.mov', '.wmv', '.mpeg', '.mpg', '.avi', '.rm'],
#     'Audio': ['.mp3','.mp4', '.wav', '.aiff', '.midi', '.m4p']
# }