# -*- coding: utf-8 -*-
from datetime import datetime

from django.contrib import admin
from django.contrib.flatpages.forms import FlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.forms.models import modelformset_factory
from hotel.models import Guest, Reservation, Room, RoomType, Building, PageImage, PeopleReport
# Register your models here.

# class MyAdminSite(admin.AdminSite):
#     site_header = 'Администрирование бронированием гостиницы Дубрава'
#
#
# admin_site = MyAdminSite(name='hotel_admin')

from tinymce.widgets import TinyMCE

ReservationFormSet = modelformset_factory(Reservation, fields='__all__')

class ReservationAdmin(admin.StackedInline):
    # extra = 1
    model = Reservation
    # formset = ReservationFormSet
    
    # def get_queryset(self, request, **kwargs):
    #     print 'make queryset'
    #     qs = super(ReservationAdmin, self).get_queryset(request, **kwargs)
    #     return qs.filter(occupied_to__lte=datetime.now)

class BuildingAdmin(admin.ModelAdmin):
    list_display = ('name',)

class RoomInlineAdmin(admin.TabularInline):
    model = Room

class RoomTypeAdmin(admin.ModelAdmin):
    inlines = (RoomInlineAdmin,)
    list_display = ('name', 'beds', 'price')


class RoomAdmin(admin.ModelAdmin):
    list_display = ('floor', 'number', 'room_type', 'is_free')
    list_filter = ('floor', 'room_type')
    search_fields = ('number',)
    inlines = (ReservationAdmin,)


class GuestAdmin(admin.ModelAdmin):
    list_display = ('full_name',)
    search_fields = ('full_name',)
    inlines = (ReservationAdmin,)


class PageImagesInline(admin.TabularInline):
    # list_display = ('page', 'image_tag')
    readonly_fields = ('image_tag',)
    model = PageImage


class RoomTypeInline(admin.TabularInline):
    # list_display = ('page', 'image_tag')
    # readonly_fields = ('image_tag',)
    model = RoomType


class PageImageAdmin(admin.ModelAdmin):
    readonly_fields = ('image_tag',)
    list_display = ('page', 'image', 'image_tag',)

#
# from django.contrib import admin
from django.contrib.flatpages.admin import FlatpageForm, FlatPageAdmin
from django.contrib.flatpages.models import FlatPage

# FlatPage.sites

class PageForm(FlatpageForm):
    class Meta:
        fields = ('url', 'title', 'content')
        # fields = '__all__'
        model = FlatPage
        widgets = {
            'content': TinyMCE(attrs={'cols': 100, 'rows': 15}),
        }

class ExtendedFlatPageAdmin(FlatPageAdmin):
    # exclude = ('enable_comments','registration_required')

    inlines = (PageImagesInline,)
    search_fields = ('title', 'url',)
    form = PageForm


#
# from models import ExtendedFlatPage
#
# class ExtendedFlatPageForm(FlatpageForm):
#     class Meta:
#         model = ExtendedFlatPage
#
# class ExtendedFlatPageAdmin(FlatPageAdmin):
#     form = ExtendedFlatPageForm
#     fieldsets = (
#         (None, {'fields': ('url', 'title', 'content', 'sites', 'order')}),
#     )
#

class PeopleReportAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
#
    list_display = ('date', 'rooms', 'people', 'confirmed', 'at_enterance_day')
    readonly_fields = ('date', 'rooms', 'people', 'confirmed', 'at_enterance_day')

    # def has_add_permission(self, request, obj=None):
    #     return False
    #
    # def has_delete_permission(self, request, obj=None):
    #     return False

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, ExtendedFlatPageAdmin)
admin.site.register(PageImage, PageImageAdmin)
admin.site.register(Guest, GuestAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(RoomType, RoomTypeAdmin)
admin.site.register(Building, BuildingAdmin)

admin.site.register(PeopleReport, PeopleReportAdmin)