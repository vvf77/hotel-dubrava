# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime, date, timedelta
from uuid import uuid1
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.contrib.flatpages.models import FlatPage
from django.db.models.query_utils import Q

from django.utils.timezone import utc
from random import shuffle


class Guest(models.Model):
    class Meta:
        verbose_name = u'Гость'
        verbose_name_plural = u"Гости"
    full_name = models.CharField(u'Полное имя', max_length=250)
    email = models.EmailField(u'E-mail', max_length=150, blank=True)
    phone = models.CharField(u'Номер телефона', max_length=20)
    user = models.OneToOneField(User, limit_choices_to={'is_staff': False}, on_delete=models.SET_NULL, null=True, blank=True)
    # if guset is trusted - than dont' need to confirm booking
    is_trusted = models.BooleanField('Не нужно подтверждение?', default=False)

    def __unicode__(self):
        return self.full_name
    __str__ = __unicode__


class Building(models.Model):
    class Meta:
        verbose_name = u'Корпус'
        verbose_name_plural = u"Корпуса"
    name = models.CharField(max_length=150)

    def __unicode__(self):
        return self.name
    __str__ = __unicode__


class RoomType(models.Model):
    class Meta:
        verbose_name = u'Тип номера'
        verbose_name_plural = u"Типы номеров"
    name = models.CharField('Наименование', max_length=50)
    # slug = models.SlugField(u'Slug', max_length=50)
    beds = models.IntegerField(u'Количество мест', choices=((2, 'двуместный'), (3, 'трехместный'), (4, '4-х местный')))
    description = models.TextField('Краткое описание', blank=True, null=False)
    price = models.FloatField(u'Цена', default=600.0)

    flatpage = models.OneToOneField(FlatPage, null=True, blank=True)
    #slug

    def __unicode__(self):
        return self.name
    __str__ = __unicode__

    @classmethod
    def available(cls, for_from=None, for_to=None):
        if for_from is None:
            for_from = datetime.today().replace(hour=0, minute=0, second=0)

        if for_to:
            for_to = for_from.replace(hour=23, minute=59, second=59)
        #models.Q(room__occupied_from__gte=for_from, room__occupied_to__lte=for_to)

        cls.objects.exclude(room__reservation__occupied_from__gte=for_from, room__reservation__occupied_to__lte=for_to)


# class ReservationReport(object):
#     class _meta:
#         app_label = 'hotel'  # This is the app that the form will exist under
#         model_name = 'custom-form'  # This is what will be used in the link url
#         verbose_name_plural = 'Custom AdminForm'  # This is the name used in the link text
#         object_name = 'ObjectName'
#
#         swapped = False
#         abstract = False
    # TODO: here make query to show data in admin panel


class Room(models.Model):
    class Meta:
        verbose_name = u'Номер'
        verbose_name_plural = u"Номера"
    building = models.ForeignKey(Building)
    floor = models.IntegerField(u'Этаж')
    number = models.CharField(u'Номер', max_length=10)
    room_type = models.ForeignKey(RoomType, verbose_name=u"Тип комнаты")

    def is_free(self, for_date=None):
        if isinstance(for_date, dict) or isinstance(for_date, tuple):
            _from = for_date
            _to = for_date
            if isinstance(for_date, dict):
                _from, _to = for_date['occupied_from'], for_date['occupied_to']
            if isinstance(for_date, tuple):
                _from, _to = for_date
            # result = self.reservation_set.filter(
            #     (Q(occupied_from__gt=_from) & Q(occupied_from__gt=_to)) |
            #     (Q(occupied_to__lt=_from) & Q(occupied_to__lt=_to))).exists()

            # result = not self.reservation_set.filter(
            #     (Q(occupied_from__lt=_from) & Q(occupied_to__gt=_from))|
            #     (Q(occupied_from__lt=_to) & Q(occupied_to__gt=_to))
            # )

            result = not self.reservation_set.filter(occupied_from__gte=_from, occupied_to__lte=_to).exists()

            return result
        if for_date is None:
            for_date = datetime.utcnow().replace(tzinfo=utc)
        return not self.reservation_set.filter(occupied_from__gte=for_date, occupied_to__lte=for_date).exists()
    is_free.boolean = True
    is_free.short_description = u'Свободен для заселения'

    @classmethod
    def available(cls, for_from=None, for_to=None):
        if for_from is None:
            for_from = datetime.today().replace(hour=12, minute=0, second=0)

        if not for_to:
            if isinstance(for_from, datetime):
                for_to = for_from.replace(hour=12, minute=59, second=59)
            else:
                for_to = for_from + ' 12:59:59'
        #models.Q(room__occupied_from__gte=for_from, room__occupied_to__lte=for_to)

        return cls.objects.exclude(reservation=Reservation.objects.filter(occupied_from__gte=for_from, occupied_to__lte=for_to))

    def __unicode__(self):
        return '{} #{}'.format(self.building.name, self.number)
    __str__ = __unicode__

def room_limiter():
    q = ~ models.Q(reservation__occupied_from__gte=datetime.now(), reservation__occupied_to__lte=datetime.now())
    return q

def generate_hash():
    return "{}".format(uuid1())


class Reservation(models.Model):
    class Meta:
        verbose_name = u'Бронь'
        verbose_name_plural = u"Брони"
    STATUSES = (('new', u'Новая'), ('confirmed', u'Подтверждена'), ('payed', u'Оплачена'), ('active', u'Активна'), ('done', u'Выполнена'))
    STATUSES_DICT = dict(STATUSES)

    room = models.ForeignKey(Room,
                             limit_choices_to=room_limiter())
    guest = models.ForeignKey(Guest)
    occupied_from = models.DateField(u'Дата вселения', db_index=True)
    occupied_to = models.DateField(u'Дата выселения', db_index=True)

    status = models.CharField(u'Статус брони', max_length=50, choices=STATUSES, default='new')
    create_time = models.DateTimeField(u'Время создания', auto_now=True, auto_now_add=True)
    confirm_code = models.CharField(u'Код подтверждения', max_length=130, default=generate_hash, db_index=True)


    people_count = models.PositiveSmallIntegerField(u'Количество гостей', default=2)

    def __unicode__(self):
        return 'Reserv id:{}, {} to {} for {}. status={}'.format(
            self.id,
            self.occupied_from.strftime('%Y.%m.%d'),
            self.occupied_to.strftime('%Y.%m.%d'),
            self.guest.full_name,
            self.status
        ) # TODO: make verbose
    __str__ = __unicode__

    def get_verbouse_status(self):
        return self.STATUSES_DICT[self.status]


class PeopleReport(models.Model):

    class Meta:
        verbose_name_plural = 'Статистика'
        verbose_name = 'Дата'
        managed = False
        db_table = 'hotel_report'
        ordering = ('date',)


    date = models.DateField(u'на дату', db_index=True)
    rooms = models.IntegerField('Комнат занято')
    people = models.IntegerField('Людей забронировано')
    confirmed = models.IntegerField('Людей подтверждено')
    at_enterance_day = models.IntegerField('Без предварительной брони')

    def __unicode__(self):
        return self.date.strftime("%d.%m.%Y")
    __str__ = __unicode__

    # def rooms(self):
    #     pass
    #
    # rooms.short_description = u'Забронированно номеров'
    #
    # def people(self):
    #     pass
    #
    # people.short_description = u'Людей'
    #
    # def confirmed(self):
    #     pass
    #
    # confirmed.short_description = u'Подтвержденных'


class PageImage(models.Model):
    class Meta:
        verbose_name_plural = 'картинки'
        verbose_name = 'картинка'

    image = models.ImageField('Файл', upload_to='page_images/', default='/media/page_images/None/no-img.jpg')
    page = models.ForeignKey(FlatPage)

    def image_tag(self):
        if self.id:
            return '<img src="{}" width=200 />'.format(self.image.url)
        return ''
    image_tag.short_description = 'Превью'
    image_tag.allow_tags = True


def generate_rooms():
    buildings = Building.objects.bulk_create([Building(name=u"Корпус {}".format(i)) for i in range(1, 3)])
    RoomType.objects.bulk_create([RoomType(
        name=u"{}-х местный{}".format(b, lx),
        beds=b) for b in (2, 3, 4) for lx in (u' Люкс', '')])
    # two buildings by 4 float by 6 rooms at floor. and 6 types: 2*4 each type
    room_types = [rt for i in range(8) for rt in RoomType.objects.all()]
    shuffle(room_types)
    buildings = list(Building.objects.all())
    print(len(buildings))
    rooms = [Room(
        room_type=room_types[i],
        number=".{}.".format(i+1),
        floor=1 + i//6 % 4,
        building=buildings[i//24]) for i in range(48)]
    Room.objects.bulk_create(rooms)


# class PageSettings(models.Model):
#     page = models.OneToOneField(FlatPage)
#     menu_level = models.IntegerField('В меню какого уровня отображать', blank=True, null=True, default=1)
#     parent_page = models.OneToOneField(FlatPage, blank=True, related_name='parent')

def do_logging_sql_to_console():
    import logging
    l = logging.getLogger('django.db.backends')
    l.addHandler(logging.StreamHandler())
    l.setLevel(logging.DEBUG)
