from django.template.defaultfilters import stringfilter
from hotel.models import RoomType

__author__ = 'vvf'

from django import template

register = template.Library()


@register.filter
def url_parts(flat_page):
    return [ p for p in flat_page.url.split('/') if p]

@register.filter
def menu_level(flat_page):
    return len([ p for p in flat_page.url.split('/') if p])

@register.filter
def is_subpage(page, subpage):
    return subpage.url.startswith(page.url)

@register.filter
def has_roomtype(page):
    return RoomType.objects.filter(flatpage=page).exists()