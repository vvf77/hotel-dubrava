# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site

from django.db import models, migrations
from hotel.models import generate_rooms


def create_needed_pages(apps, schema_editor):
    # FlatPage = apps.get_model("django.contrib.flatpage", "FlatPage")
    # Site = apps.get_model("django.contrib.sites", "Site")
    site = Site.objects.first()
    FlatPage.objects.bulk_create([
        FlatPage(url='/', title='', content=''),
        FlatPage(url='/booking/', title='Бронирование номера', content=''),
    ])
    if site:
        site.flatpage_set = FlatPage.objects.all()
    generate_rooms()

def rollback_creating_pages(apps, schema_editor):
    pass

class Migration(migrations.Migration):

    dependencies = [
        ('hotel', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_needed_pages, reverse_code=rollback_creating_pages),
        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION next14days()
  RETURNS SETOF date AS
$BODY$DECLARE
  dt date;
  dt_to date;
BEGIN
  SELECT CAST( NOW() AS DATE) - 7 INTO dt;
  SELECT max(occupied_to) FROM hotel_reservation INTO dt_to;
  IF dt_to IS NULL OR dt_to < dt THEN
	dt_to = dt + 30;
  END IF;
  WHILE dt < dt_to LOOP
	RETURN NEXT dt;
	dt = dt + 1;
  END LOOP;
END$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
""", """
DROP FUNCTION next14days()
        """),
        migrations.RunSQL("""
CREATE OR REPLACE VIEW hotel_report AS
 SELECT EXTRACT(DOY FROM d)::Integer as "id",
    d.d AS "date",
    count(r.id) AS rooms,
    sum(r.people_count) AS people,
    sum(
        CASE
            WHEN r.status::text <> 'new'::text THEN r.people_count::integer
            ELSE 0
        END) AS confirmed,
    sum(
        CASE
            WHEN date_trunc('day', r.create_time) = r.occupied_from
		AND r.occupied_from = d.d
		THEN r.people_count::integer
            ELSE 0
        END) AS at_enterance_day
   FROM next14days() d(d)
     LEFT JOIN hotel_reservation r ON d.d >= r.occupied_from AND d.d <= r.occupied_to
  GROUP BY d.d
  ORDER BY d.d;
""", """
DROP VIEW hotel_report
        """),

    ]
