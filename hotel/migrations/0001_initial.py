# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings
import hotel.models


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Building',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
            ],
            options={
                'verbose_name': '\u041a\u043e\u0440\u043f\u0443\u0441',
                'verbose_name_plural': '\u041a\u043e\u0440\u043f\u0443\u0441\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Guest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('full_name', models.CharField(max_length=250, verbose_name='\u041f\u043e\u043b\u043d\u043e\u0435 \u0438\u043c\u044f')),
                ('email', models.EmailField(max_length=150, verbose_name='E-mail', blank=True)),
                ('phone', models.CharField(max_length=20, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430')),
                ('is_trusted', models.BooleanField(default=False, verbose_name='\u041d\u0435 \u043d\u0443\u0436\u043d\u043e \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u0438\u0435?')),
                ('user', models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': '\u0413\u043e\u0441\u0442\u044c',
                'verbose_name_plural': '\u0413\u043e\u0441\u0442\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PageImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(default='/media/page_images/None/no-img.jpg', upload_to='page_images/', verbose_name='\u0424\u0430\u0439\u043b')),
                ('page', models.ForeignKey(to='flatpages.FlatPage')),
            ],
            options={
                'verbose_name': '\u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0430',
                'verbose_name_plural': '\u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PeopleReport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day', models.DateField(verbose_name='\u043d\u0430 \u0434\u0430\u0442\u0443', db_index=True)),
            ],
            options={
                'verbose_name': '\u0414\u0430\u0442\u0430',
                'verbose_name_plural': '\u0414\u0430\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('occupied_from', models.DateField(verbose_name='\u0414\u0430\u0442\u0430 \u0432\u0441\u0435\u043b\u0435\u043d\u0438\u044f', db_index=True)),
                ('occupied_to', models.DateField(verbose_name='\u0414\u0430\u0442\u0430 \u0432\u044b\u0441\u0435\u043b\u0435\u043d\u0438\u044f', db_index=True)),
                ('status', models.CharField(default='new', max_length=50, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u0431\u0440\u043e\u043d\u0438', choices=[('new', '\u041d\u043e\u0432\u0430\u044f'), ('confirmed', '\u041f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u0430'), ('payed', '\u041e\u043f\u043b\u0430\u0447\u0435\u043d\u0430'), ('active', '\u0410\u043a\u0442\u0438\u0432\u043d\u0430'), ('done', '\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0430')])),
                ('create_time', models.DateTimeField(auto_now=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f', auto_now_add=True)),
                ('confirm_code', models.CharField(default=hotel.models.generate_hash, max_length=130, verbose_name='\u041a\u043e\u0434 \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u0438\u044f', db_index=True)),
                ('people_count', models.PositiveSmallIntegerField(default=2, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0433\u043e\u0441\u0442\u0435\u0439')),
                ('guest', models.ForeignKey(to='hotel.Guest')),
            ],
            options={
                'verbose_name': '\u0411\u0440\u043e\u043d\u044c',
                'verbose_name_plural': '\u0411\u0440\u043e\u043d\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('floor', models.IntegerField(verbose_name='\u042d\u0442\u0430\u0436')),
                ('number', models.CharField(max_length=10, verbose_name='\u041d\u043e\u043c\u0435\u0440')),
                ('building', models.ForeignKey(to='hotel.Building')),
            ],
            options={
                'verbose_name': '\u041d\u043e\u043c\u0435\u0440',
                'verbose_name_plural': '\u041d\u043e\u043c\u0435\u0440\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RoomType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('beds', models.IntegerField(verbose_name='\u0421\u043a\u043e\u043b\u044c\u043a\u0438\u043c\u0435\u0441\u0442\u043d\u044b\u0439', choices=[(2, '\u0434\u0432\u0443\u043c\u0435\u0441\u0442\u043d\u044b\u0439'), (3, '\u0442\u0440\u0435\u0445\u043c\u0435\u0441\u0442\u043d\u044b\u0439'), (4, '4-\u0445 \u043c\u0435\u0441\u0442\u043d\u044b\u0439')])),
                ('description', models.TextField(verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('price', models.FloatField(default=600.0, verbose_name='\u0426\u0435\u043d\u0430')),
                ('flatpage', models.OneToOneField(null=True, blank=True, to='flatpages.FlatPage')),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u043d\u043e\u043c\u0435\u0440\u0430',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u043d\u043e\u043c\u0435\u0440\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='room',
            name='room_type',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u043a\u043e\u043c\u043d\u0430\u0442\u044b', to='hotel.RoomType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='reservation',
            name='room',
            field=models.ForeignKey(to='hotel.Room'),
            preserve_default=True,
        ),
    ]
