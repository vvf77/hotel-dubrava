# -*- coding: utf-8 -*-
from random import shuffle

__author__ = 'vvf'

from django.test import TestCase
from hotel.models import *
from django.test import Client

class AnimalTestCase(TestCase):
    def setUp(self):
        #TODO: generate building, roomtypes and rooms
        generate_rooms()

    def test_new_guest_booking(self):
        rt = RoomType.objects.order_by('?').first()
        c = Client()
        rooms = Room.available('2015-04-10', '2015-04-15').filter(room_type=rt)
        response = c.post('/booking/', dict(room_type=rt.id, occupied_from='10.04.2015', occupied_to='15.04.2015', people_count=2))
        print(response.context['rooms'])
        assert(list(response.context['rooms']) == list(rooms))
        response = c.post('/booking/', dict(room_type=rt.id, occupied_from='10.04.2015', occupied_to='15.04.2015', people_count=2))

        print(response.context['rooms'])
        assert(list(response.context['rooms']) == list(rooms))
